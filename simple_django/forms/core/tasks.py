from core.models import Logger
from core.utils import get_mono_bank_currency, get_vkurse_currency, get_kurs_currency, get_minfin_currency, get_industrial_bank_currency
from django.core.mail import send_mail
from datetime import datetime, timedelta
from forms import celery_app
import pytz


@celery_app.task
def send_mail_celery(title, sender, message):
    send_mail(subject=title, message=message, from_email=sender, recipient_list=[sender])
    print("Mail sent!")


@celery_app.task
def check_date():
    now = datetime.now()
    now = pytz.utc.localize(now)
    Logger.objects.filter(time_created__lte=now - timedelta(days=7)).delete()


@celery_app.task
def store_currency():
    get_mono_bank_currency()
    get_vkurse_currency()
    get_kurs_currency()
    get_minfin_currency()
    get_industrial_bank_currency()
